<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NigmaCode Designs</title>
    <link rel="stylesheet" href="{!! asset('css/styles.css') !!}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Bungee&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
</head>
<body>
    <div id="page" class="wrapper">
        <div class="container-fluid">
            <div class="row text-center">
              <div class="col-12">
                <h1 id="nameweb" class="name-web">NigmaCode Designs</h1>
              </div>
              <div class="row sections">
                <div class="col-4">
                  <div class="card">
                      <h2>Proyectos</h2>
                      <img src="{!! asset('img/1.jpg') !!}" alt="1">
                  </div>
                </div>
                <div class="col-4">
                  <div class="card">
                      <h2>Acerca de</h2>
                      <img src="{!! asset('img/2.jpg') !!}" alt="1">
                  </div>
                </div>
                <div class="col-4">
                  <div class="card">
                      <h2>Blog</h2>
                      <img src="{!! asset('img/3.jpg') !!}" alt="1">
                  </div>
                </div>
                <div class="col-12">
                  <div class="btn-contact">Contacto</div>
                </div>
              </div>
            </div>
        </div>
        {{-- Boton de cambio de modo --}}
        <div class="container-btn-mode">
            <div id="id-sun" class="btn-mode sun active">
               <i class="fas fa-sun"></i>
            </div>
            <div id="id-moon" class="btn-mode moon">
               <i class="fas fa-moon"></i>
            </div>
        </div>
    </div>
    
</body>

<script src="{!! asset('js/index.js') !!}"></script>
</html>